import random as r

piece_score = {"K": 0, "Q": 100, "R": 50, "B": 30, "N": 30, "p": 10}
CHECK_MATE = 1000
STALE_MATE = 0
DEPTH = 3
next_move = None
c = {'wQ': 0, 'wK': 0, 'wB': 0, 'wN': 0, 'wR': 0, 'wp': 0,
     'bQ': 0, 'bK': 0, 'bB': 0, 'bN': 0, 'bR': 0, 'bp': 0}

pawn_table = [0, 0, 0, 0, 0, 0, 0, 0,
              50, 50, 50, 50, 50, 50, 50, 50,
              10, 10, 20, 30, 30, 20, 10, 10,
              5, 5, 10, 25, 25, 10, 5, 5,
              0, 0, 0, 20, 20, 0, 0, 0,
              5, -5, -10, 0, 0, -10, -5, 5,
              5, 10, 10, -20, -20, 10, 10, 5,
              0, 0, 0, 0, 0, 0, 0, 0]
knight_table = [-50, -40, -30, -30, -30, -30, -40, -50,
                -40, -20, 0, 0, 0, 0, -20, -40,
                -30, 0, 10, 15, 15, 10, 0, -30,
                -30, 5, 15, 20, 20, 15, 5, -30,
                -30, 0, 15, 20, 20, 15, 0, -30,
                -30, 5, 10, 15, 15, 10, 5, -30,
                -40, -20, 0, 5, 5, 0, -20, -40,
                -50, -90, -30, -30, -30, -30, -90, -50]
bishop_table = [-20, -10, -10, -10, -10, -10, -10, -20,
                -10, 0, 0, 0, 0, 0, 0, -10,
                -10, 0, 5, 10, 10, 5, 0, -10,
                -10, 5, 5, 10, 10, 5, 5, -10,
                -10, 0, 10, 10, 10, 10, 0, -10,
                -10, 10, 10, 10, 10, 10, 10, -10,
                -10, 5, 0, 0, 0, 0, 5, -10,
                -20, -10, -90, -10, -10, -90, -10, -20]
rook_table = [0, 0, 0, 0, 0, 0, 0, 0,
              5, 10, 10, 10, 10, 10, 10, 5,
              -5, 0, 0, 0, 0, 0, 0, -5,
              -5, 0, 0, 0, 0, 0, 0, -5,
              -5, 0, 0, 0, 0, 0, 0, -5,
              -5, 0, 0, 0, 0, 0, 0, -5,
              -5, 0, 0, 0, 0, 0, 0, -5,
              0, 0, 0, 5, 5, 0, 0, 0]
queen_table = [-20, -10, -10, -5, -5, -10, -10, -20,
               -10, 0, 0, 0, 0, 0, 0, -10,
               -10, 0, 5, 5, 5, 5, 0, -10,
               -5, 0, 5, 5, 5, 5, 0, -5,
               0, 0, 5, 5, 5, 5, 0, -5,
               -10, 5, 5, 5, 5, 5, 0, -10,
               -10, 0, 5, 0, 0, 0, 0, -10,
               -20, -10, -10, 70, -5, -10, -10, -20]
king_table = [-30, -40, -40, -50, -50, -40, -40, -30,
              -30, -40, -40, -50, -50, -40, -40, -30,
              -30, -40, -40, -50, -50, -40, -40, -30,
              -30, -40, -40, -50, -50, -40, -40, -30,
              -20, -30, -30, -40, -40, -30, -30, -20,
              -10, -20, -20, -20, -20, -20, -20, -10,
              20, 20, 0, 0, 0, 0, 20, 20,
              20, 30, 10, 0, 0, 10, 30, 20]
king_endgame_table = [-50, -40, -30, -20, -20, -30, -40, -50,
                      -30, -20, -10, 0, 0, -10, -20, -30,
                      -30, -10, 20, 30, 30, 20, -10, -30,
                      -30, -10, 30, 40, 40, 30, -10, -30,
                      -30, -10, 30, 40, 40, 30, -10, -30,
                      -30, -10, 20, 30, 30, 20, -10, -30,
                      -30, -30, 0, 0, 0, 0, -30, -30,
                      -50, -30, -30, -30, -30, -30, -30, -50]

piece_pos_score = {'N': knight_table, 'p': pawn_table, 'K': king_table, 'Q': queen_table, "R": rook_table, "B": bishop_table}


def random_move(valid_moves):
    random = r.randint(0, len(valid_moves) - 1)
    return valid_moves[random]


'''
larger score means good for white and smaller score means good for black
'''


def advance_eval(engine):
    if engine.check_mate:  # being checkmated
        if engine.player:  # and the next turn is white to move then black win
            return -CHECK_MATE
        else:
            return CHECK_MATE  # the next turn is black to move then white win
    elif engine.stale_mate:
        return 0  # no one win this game so it's not so bad yet not so good
    score = 0
    piece_pos_scored = 0
    for row in range(len(engine.board)):
        for col in range(len(engine.board[row])):
            square = engine.board[row][col]
            if square != "--":
                piece_pos_scored = 0
                if square[0] == 'N':
                    piece_pos_scored = piece_pos_score["N"][row][col]
                elif square[0] == 'R':
                    piece_pos_scored = piece_pos_score["R"][row][col]
                elif square[0] == 'B':
                    piece_pos_scored = piece_pos_score["B"][row][col]
                elif square[0] == 'p':
                    piece_pos_scored = piece_pos_score["p"][row][col]
                elif square[0] == 'K':
                    piece_pos_scored = piece_pos_score["K"][row][col]
                elif square[0] == 'Q':
                    piece_pos_scored = piece_pos_score["Q"][row][col]
            if square[0] == 'w':
                score += piece_score[square[1]] + piece_pos_scored
            elif square[0] == 'b':
                score -= piece_score[square[1]] + (-piece_pos_scored)
    return score


def negamax(engine, valid_moves, depth, alpha, beta, turn):
    global next_move
    if depth == 0:
        return turn * advance_eval(engine)
    max_score = -CHECK_MATE
    for move in valid_moves:
        pos = (move.end_row, move.end_col)
        engine.make_demo_move(move)
        next_valid_moves = engine.get_valid_moves()
        score = - negamax(engine, next_valid_moves, depth - 1, -beta, -alpha, -turn)
        # print(depth)
        if score > max_score:
            max_score = score
            if depth == DEPTH:
                next_move = move
        engine.undo_demo_move()
        if max_score > alpha:
            alpha = max_score
        if alpha >= beta:
            break
    return max_score


def find_minimax_best_move(engine, valid_moves):
    global next_move
    r.shuffle(valid_moves)
    print(negamax(engine, valid_moves, DEPTH, -1000, 1000, 1 if engine.player else -1))
    return next_move

# def best_move(engine, valid_moves):
#     max_score = CHECK_MATE
#     best = None
#     turn = 1 if engine.player else -1
#     r.shuffle(valid_moves)
#     for player_move in valid_moves:
#         engine.make_demo_move(player_move)
#         opponent_moves = engine.get_valid_moves()
#         opponent_max_score = -CHECK_MATE
#         for opponent_move in opponent_moves:
#             engine.make_demo_move(opponent_move)
#             if engine.check_mate:
#                 score = -turn * CHECK_MATE
#             elif engine.stale_mate:
#                 score = STALE_MATE
#             else:
#                 score = - evaluate(engine.board) * turn
#             if score > opponent_max_score:
#                 opponent_max_score = score
#             engine.undo_demo_move()
#         if max_score > opponent_max_score:
#             max_score = opponent_max_score
#             best = player_move
#         engine.undo_demo_move()
#     return best


def minimax(engine, valid_moves, depth, player, alpha, beta):
    global next_move
    print(player)
    if depth == 0:
        return advance_eval(engine)
    if player:  # if the player is white then we will try to maximize the score
        max_score = -CHECK_MATE
        for move in valid_moves:
            engine.make_demo_move(move)
            next_valid_moves = engine.get_valid_moves()
            score = minimax(engine, next_valid_moves, depth - 1, not player, alpha, beta)
            if score > max_score:
                max_score = score
                # print("max ----> " + depth)
                if depth == DEPTH:
                    next_move = move
                    print(next_move)
            # max_score = max(max_score, score)
            engine.undo_demo_move()
            # pruning
            # alpha = max(alpha, max_score)
            # if beta <= alpha:
            #     break
        return max_score

    else:  # if the player is black then we will try to minimize the score
        min_score = CHECK_MATE
        for move in valid_moves:
            engine.make_demo_move(move)
            next_valid_moves = engine.get_valid_moves()
            score = minimax(engine, next_valid_moves, depth - 1, not player, alpha, beta)
            min_score = min(min_score, score)

            if score < min_score:
                min_score = score
                # print("min ----> "+ depth)
                if depth == DEPTH:
                    next_move = move
                    print(next_move)
            # beta = min(beta, min_score)
            # if beta <= alpha:
            #     break
            engine.undo_demo_move()
        return min_score
