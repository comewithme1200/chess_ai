WIDTH = HEIGHT = 512
DIMENSION = 8
SQ_SIZE = HEIGHT // DIMENSION
MAX_FPS = 20
IMAGES = {}
WHITE_WIN = "White wins by checkmate"
BLACK_WIN = "Black wins by checkmate"
STALE_MATE = "Stale mate"
