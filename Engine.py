class GameState:
    def __init__(self):
        # Board is an 8x8 2d array, represent the first state of a chess game
        self.board = [
            ["bR", "bN", "bB", "bQ", "bK", "bB", "bN", "bR"],
            ["bp", "bp", "bp", "bp", "bp", "bp", "bp", "bp"],
            ["--", "--", "--", "--", "--", "--", "--", "--"],
            ["--", "--", "--", "--", "--", "--", "--", "--"],
            ["--", "--", "--", "--", "--", "--", "--", "--"],
            ["--", "--", "--", "--", "--", "--", "--", "--"],
            ["wp", "wp", "wp", "wp", "wp", "wp", "wp", "wp"],
            ["wR", "wN", "wB", "wQ", "wK", "wB", "wN", "wR"]]

        self.move_fn = {'p': self.get_pawn_move, 'R': self.get_rook_move, 'N': self.get_knight_move,
                        'B': self.get_bishop_move, 'Q': self.get_queen_move, 'K': self.get_king_move}
        self.player = True
        self.move_log = []
        self.white_king_pos = (7, 4)
        self.black_king_pos = (0, 4)
        self.check_mate = False
        self.stale_mate = False
        self.castle_state = CastlePrinciple(True, True, True, True)
        self.castle_log = [CastlePrinciple(self.castle_state.white_king_side, self.castle_state.black_king_side,
                                           self.castle_state.white_queen_side, self.castle_state.black_queen_side)]

    def make_move(self, move):
        self.board[move.start_row][move.start_col] = "--"
        self.board[move.end_row][move.end_col] = move.piece_moved
        self.move_log.append(move)
        self.player = not self.player
        # update king's pos
        if move.piece_moved == 'wK':
            self.white_king_pos = (move.end_row, move.end_col)
        if move.piece_moved == 'bK':
            self.black_king_pos = (move.end_row, move.end_col)

        # if pawn promotion
        if move.is_promoted:
            self.board[move.end_row][move.end_col] = move.piece_moved[0] + 'Q'

        # make the castle move

        if move.is_castle:
            if move.end_col - move.start_col == 2:
                self.board[move.end_row][move.end_col - 1] = self.board[move.end_row][move.end_col + 1]
                self.board[move.end_row][move.end_col + 1] = "--"
            else:
                self.board[move.end_row][move.end_col + 1] = self.board[move.end_row][move.end_col - 2]
                self.board[move.end_row][move.end_col - 2] = "--"
        # update the castle state
        self.update_castle_principle(move)
        self.castle_log.append(CastlePrinciple(self.castle_state.white_king_side, self.castle_state.black_king_side,
                                               self.castle_state.white_queen_side, self.castle_state.black_queen_side))

    def make_demo_move(self, move):
        self.board[move.start_row][move.start_col] = "--"
        self.board[move.end_row][move.end_col] = move.piece_moved
        self.move_log.append(move)
        self.player = not self.player
        # update king's pos
        if move.piece_moved == 'wK':
            self.white_king_pos = (move.end_row, move.end_col)
        if move.piece_moved == 'bK':
            self.black_king_pos = (move.end_row, move.end_col)

    def undo_demo_move(self):
        if len(self.move_log) != 0:
            move = self.move_log.pop()
            self.board[move.start_row][move.start_col] = move.piece_moved
            self.board[move.end_row][move.end_col] = move.piece_captured
            self.player = not self.player
            # update king's pos
            if move.piece_moved == 'wK':
                self.white_king_pos = (move.start_row, move.start_col)
            if move.piece_moved == 'bK':
                self.black_king_pos = (move.start_row, move.start_col)

    def undo_move(self):
        if len(self.move_log) != 0:
            move = self.move_log.pop()
            self.board[move.start_row][move.start_col] = move.piece_moved
            self.board[move.end_row][move.end_col] = move.piece_captured
            self.player = not self.player
            # update king's pos
            if move.piece_moved == 'wK':
                self.white_king_pos = (move.start_row, move.start_col)
            if move.piece_moved == 'bK':
                self.black_king_pos = (move.start_row, move.start_col)

            # undo castle
            self.castle_log.pop()
            self.castle_state.black_queen_side = self.castle_log[-1].black_queen_side
            self.castle_state.black_king_side = self.castle_log[-1].black_king_side
            self.castle_state.white_queen_side = self.castle_log[-1].white_queen_side
            self.castle_state.white_king_side = self.castle_log[-1].white_king_side

            if move.is_castle:
                if move.end_col - move.start_col == 2:
                    self.board[move.end_row][move.end_col + 1] = self.board[move.end_row][move.end_col - 1]
                    self.board[move.end_row][move.end_col - 1] = "--"
                else:
                    self.board[move.end_row][move.end_col - 2] = self.board[move.end_row][move.end_col + 1]
                    self.board[move.end_row][move.end_col + 1] = "--"

    def update_castle_principle(self, move):
        if move.piece_moved == 'wK':
            print("vua trắng di chuyển")
            self.castle_state.white_queen_side = False
            self.castle_state.white_king_side = False
        elif move.piece_moved == 'bK':
            print("vua đen di chuyển")
            self.castle_state.black_queen_side = False
            self.castle_state.black_king_side = False
        elif move.piece_moved == 'wR':
            if move.start_row == 7:
                if move.start_col == 0:
                    print("xe trắng di chuyển")
                    self.castle_state.white_queen_side = False
                elif move.start_col == 7:
                    print("xe trắng di chuyển")
                    self.castle_state.white_king_side = False
        elif move.piece_moved == 'bR':
            if move.start_row == 0:
                if move.start_col == 0:
                    print("xe đen di chuyển")
                    self.castle_state.black_queen_side = False
                elif move.start_col == 7:
                    print("xe đen di chuyển")
                    self.castle_state.black_king_side = False
        elif move.piece_captured == 'wR':
            if move.end_row == 7:
                if move.end_col == 0:
                    print("xe trắng bị ăn")
                    self.castle_state.white_queen_side = False
                elif move.end_col == 7:
                    print("xe trắng bị ăn")
                    self.castle_state.white_king_side = False
        elif move.piece_captured == 'bR':
            if move.end_row == 0:
                if move.end_col == 0:
                    print("xe đen di chuyển")
                    self.castle_state.black_queen_side = False
                elif move.end_col == 7:
                    print("xe đen di chuyển")
                    self.castle_state.black_king_side = False

    def get_all_possible_move(self):
        moves = []
        for row in range(len(self.board)):
            for col in range(len(self.board[row])):
                turn = self.board[row][col][0]
                if (turn == 'w' and self.player) or (turn == 'b' and not self.player):
                    piece = self.board[row][col][1]
                    self.move_fn[piece](row, col, moves)
        return moves

    def get_valid_moves(self):
        # print(self.castle_log)
        # for log in self.castle_log:
        #     print(log.white_king_side, log.white_queen_side, log.black_king_side, log.black_queen_side)
        # print("---------------------------")
        # print(self.board)
        # print(self.castle_state.white_king_side, self.castle_state.white_queen_side, self.castle_state.black_king_side, self.castle_state.black_king_side, end=', ')
        # a copy of our castle state cause a don't want the old state get lost after getting al the possible move
        temp_castle_state = CastlePrinciple(self.castle_state.white_king_side, self.castle_state.black_king_side,
                                            self.castle_state.white_queen_side, self.castle_state.black_queen_side)
        # generate all possible moves
        moves = self.get_all_possible_move()
        # print(self.player)
        if self.player:
            self.castle_move(self.white_king_pos[0], self.white_king_pos[1], moves)
        else:
            self.castle_move(self.black_king_pos[0], self.black_king_pos[1], moves)
        # make a move for each opponent move
        for i in range(len(moves) - 1, -1, -1):
            self.make_demo_move(moves[i])
            # self.make_move(moves[i])
            self.player = not self.player
            if self.in_check():
                moves.remove(moves[i])
            self.player = not self.player
            # self.undo_move()
            self.undo_demo_move()
        if len(moves) == 0:
            if self.in_check():
                self.check_mate = True
            else:
                self.stale_mate = True
        else:
            self.check_mate = False
            self.stale_mate = False
        self.castle_state = temp_castle_state
        # print(self.move_log)
        return moves

    # def order_move(self, valid_moves):
    #     ordered_move = []
    #     j = 0
    #     maximum = self.eval_move(valid_moves[0])
    #     for i in range(1, len(valid_moves)):
    #         if self.eval_move(valid_moves[i]) > maximum:
    #             maximum = self.eval_move(valid_moves[i])
    #             j = i
    #     ordered_move.append(valid_moves[j])
    #     for i in range(len(valid_moves)):
    #         if valid_moves[i] != valid_moves[j]:
    #             ordered_move.append(valid_moves[i])
    #     return ordered_move
    #
    #
    #
    # def eval_move(self, valid_moves):
    #     move_score = 0
    #     if move.piece_captured != "--":
    #         move_score = 10 * self.get_piece_price(move.piece_captured) - self.get_piece_price(move.piece_moved)
    #     if move.is_promoted:
    #         move_score = 10 * self.get_piece_price(move.piece_captured) + 100
    #     return move_score
    #
    # def get_piece_price(self, piece):
    #     price = 0
    #     if piece[1] == "Q":
    #         price += 100
    #     elif piece[1] == "R":
    #         price += 50
    #     elif piece[1] == "N":
    #         price += 30
    #     elif piece[1] == "B":
    #         price += 30
    #     elif piece[1] == "p":
    #         price += 10
    #     return price

    '''
    Find out if current player is in checked
    '''

    def in_check(self):
        if self.player:
            return self.sq_attacked(self.white_king_pos[0], self.white_king_pos[1])
        else:
            return self.sq_attacked(self.black_king_pos[0], self.black_king_pos[1])
        pass

    '''
    find out if enemy can attack a square
    '''

    def sq_attacked(self, r, c):
        self.player = not self.player
        op_moves = self.get_all_possible_move()
        self.player = not self.player
        for move in op_moves:
            if move.end_row == r and move.end_col == c:
                return True
        return False
        pass

    '''
    Get all the pawn possible moves at row and column
    '''

    def get_pawn_move(self, r, c, moves):
        if self.player:  # white player
            if self.board[r - 1][c] == "--":
                moves.append(Move((r, c), (r - 1, c), self.board))
                if r == 6 and self.board[r - 2][c] == "--":
                    moves.append(Move((r, c), (r - 2, c), self.board))
            if c - 1 >= 0:
                if self.board[r - 1][c - 1][0] == 'b':
                    moves.append(Move((r, c), (r - 1, c - 1), self.board))  # capture to the left diagnose
            if c + 1 <= 7:
                if self.board[r - 1][c + 1][0] == 'b':
                    moves.append(Move((r, c), (r - 1, c + 1), self.board))  # capture to the right diagnose
        else:
            if self.board[r + 1][c] == "--":
                moves.append(Move((r, c), (r + 1, c), self.board))
                if r == 1 and self.board[r + 2][c] == "--":
                    moves.append(Move((r, c), (r + 2, c), self.board))
            if c - 1 >= 0:
                if self.board[r + 1][c - 1][0] == 'w':
                    moves.append(Move((r, c), (r + 1, c - 1), self.board))  # capture to the left diagnose
            if c + 1 <= 7:
                if self.board[r + 1][c + 1][0] == 'w':
                    moves.append(Move((r, c), (r + 1, c + 1), self.board))  # capture to the right diagnose
                    '''
                       Get all the rook possible moves at row and col 
                    '''

    def get_rook_move(self, r, c, moves):
        direc = ((-1, 0), (0, -1), (1, 0), (0, 1))
        if self.player:
            enemy = 'b'
        else:
            enemy = 'w'
        for d in direc:
            for i in range(1, 8):
                end_row = r + d[0] * i
                end_col = c + d[1] * i
                if 0 <= end_col <= 7 and 0 <= end_row <= 7:  # the piece is not allow to go outside the board
                    end_piece = self.board[end_row][end_col]
                    if end_piece == "--":  # if nothing there
                        moves.append(Move((r, c), (end_row, end_col), self.board))
                    elif end_piece[0] == enemy:  # if there is an enemy piece on the end position
                        moves.append(Move((r, c), (end_row, end_col), self.board))
                        break
                    else:  # friendly piece
                        break
                else:  # go out of the board
                    break
        pass

    '''
    Get all the knight possible moves at row and col 
    '''

    def get_knight_move(self, r, c, moves):
        direc = ((-2, -1), (-2, 1), (-1, -2), (-1, 2), (1, -2), (1, 2), (2, -1), (2, 1))
        if self.player:
            ally = 'w'
        else:
            ally = 'b'
        for d in direc:
            end_row = r + d[0]
            end_col = c + d[1]
            if 0 <= end_col <= 7 and 0 <= end_row <= 7:
                end_piece = self.board[end_row][end_col]
                if end_piece[0] != ally:
                    moves.append(Move((r, c), (end_row, end_col), self.board))
        pass

    '''
    Get all the bishop possible moves at row and col 
    '''

    def get_bishop_move(self, r, c, moves):
        direc = ((-1, -1), (-1, 1), (1, -1), (1, 1))
        if self.player:
            enemy = 'b'
        else:
            enemy = 'w'
        for d in direc:
            for i in range(1, 8):
                end_row = r + d[0] * i
                end_col = c + d[1] * i
                if 0 <= end_col <= 7 and 0 <= end_row <= 7:  # the piece is not allow to go outside the board
                    end_piece = self.board[end_row][end_col]
                    if end_piece == "--":  # if nothing there
                        moves.append(Move((r, c), (end_row, end_col), self.board))
                    elif end_piece[0] == enemy:  # if there is an enemy piece on the end position
                        moves.append(Move((r, c), (end_row, end_col), self.board))
                        break
                    else:  # friendly piece
                        break
                else:  # go out of the board
                    break
        pass

    '''
    Get all the queen possible moves at row and col 
    '''

    def get_queen_move(self, r, c, moves):
        self.get_rook_move(r, c, moves)
        self.get_bishop_move(r, c, moves)
        pass

    '''
       Get all the king possible moves at row and col 
    '''

    def get_king_move(self, r, c, moves):
        direc = ((-1, - 1), (-1, 0), (-1, 1), (0, - 1), (0, 1), (1, - 1), (1, 0), (1, 1))
        if self.player:
            ally = 'w'
        else:
            ally = 'b'
        for i in range(8):
            end_row = r + direc[i][0]
            end_col = c + direc[i][1]
            if 0 <= end_col <= 7 and 0 <= end_row <= 7:
                end_piece = self.board[end_row][end_col]
                if end_piece[0] != ally:
                    moves.append(Move((r, c), (end_row, end_col), self.board))

        pass

    def castle_move(self, r, c, moves):
        if self.in_check():
            return
        if (self.player and self.castle_state.white_king_side) or (
                not self.player and self.castle_state.black_king_side):
            # print(str(r) + str(c))
            self.get_castle_king_side_move(r, c, moves)
        if (self.player and self.castle_state.white_queen_side) or (
                not self.player and self.castle_state.black_queen_side):
            self.get_castle_queen_side_move(r, c, moves)

    def get_castle_king_side_move(self, r, c, moves):
        if self.board[r][c + 1] == "--" and self.board[r][c + 2] == "--":
            if not self.sq_attacked(r, c + 1) and not self.sq_attacked(r, c + 2):
                moves.append(Move((r, c), (r, c + 2), self.board, is_castle=True))
        pass

    def get_castle_queen_side_move(self, r, c, moves):
        if self.board[r][c - 1] == "--" and self.board[r][c - 2] == "--" and self.board[r][c - 3] == '--' and \
                self.board[r][c - 4][1] == "R" and self.board[r][c - 4][0] == self.board[r][c][0]:
            if not self.sq_attacked(r, c - 1) and not self.sq_attacked(r, c - 2):
                moves.append(Move((r, c), (r, c - 2), self.board, is_castle=True))
        pass


class Move:
    ranks_to_rows = {"1": 7, "2": 6, "3": 5, "4": 4,
                     "5": 3, "6": 2, "7": 1, "8": 0}
    rows_to_ranks = {v: k for k, v in ranks_to_rows.items()}

    files_to_columns = {"a": 0, "b": 1, "c": 2, "d": 3,
                        "e": 4, "f": 5, "g": 6, "h": 7}

    columns_to_files = {v: k for k, v in files_to_columns.items()}

    def __init__(self, start, end, board, is_castle=False):
        self.start_row = start[0]
        self.start_col = start[1]
        self.end_row = end[0]
        self.end_col = end[1]
        self.piece_moved = board[self.start_row][self.start_col]
        self.piece_captured = board[self.end_row][self.end_col]
        self.move_static = str(self.start_row) + str(self.start_col) + str(self.end_row) + str(self.end_col)
        self.is_promoted = False
        self.is_castle = is_castle
        if (self.piece_moved == 'wp' and self.end_row == 0) or (self.piece_moved == 'bp' and self.end_row == 7):
            self.is_promoted = True

    '''
    Define how to compare move to another
    '''

    def __eq__(self, other):
        if isinstance(other, Move):
            return self.move_static == other.move_static
        return False

    def get_rank_file(self, r, c):
        return self.columns_to_files[c] + self.rows_to_ranks[r]

    def get_notation(self):
        return self.get_rank_file(self.start_row, self.start_col) + " ---> " + self.get_rank_file(self.end_row,
                                                                                                  self.end_col)


class CastlePrinciple:
    def __init__(self, white_king_side, black_king_side, white_queen_side, black_queen_side):
        self.black_king_side = black_king_side
        self.black_queen_side = black_queen_side
        self.white_king_side = white_king_side
        self.white_queen_side = white_queen_side
