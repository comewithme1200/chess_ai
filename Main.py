import Engine
import pygame as p
import Constraint as c
import BestMove

def load_images():
    pieces = ['wp', 'wR', 'wN', 'wB', 'wQ', 'wK', 'bp', 'bR', 'bN', 'bB', 'bQ', 'bK']
    for piece in pieces:
        # print("images/" + piece + ".png")
        c.IMAGES[piece] = p.transform.scale(p.image.load("images/" + piece + ".png"), (c.SQ_SIZE, c.SQ_SIZE))


def main():
    p.init()
    screen = p.display.set_mode((c.WIDTH, c.HEIGHT))
    clock = p.time.Clock()
    screen.fill(p.Color("white"))
    engine = Engine.GameState()
    valid_moves = engine.get_valid_moves()
    print(valid_moves)
    move_made = False  #
    load_images()
    is_running = True
    sq_selected = ()
    player_clicked = []
    game_over = False
    white = True  # if human is playing white
    black = False  # if human is playing black
    while is_running:
        is_human = (engine.player and white) or (not engine.player and black)
        for e in p.event.get():
            if e.type == p.QUIT:
                is_running = False
            elif e.type == p.MOUSEBUTTONDOWN:
                if not game_over and is_human:
                    location = p.mouse.get_pos()
                    col = location[0] // c.SQ_SIZE
                    row = location[1] // c.SQ_SIZE
                    if sq_selected == (row, col):  # check if the player click on the same sq
                        sq_selected = ()
                        player_clicked = []
                    else:
                        sq_selected = (row, col)
                        player_clicked.append(sq_selected)
                    if len(player_clicked) == 2:
                        move = Engine.Move(player_clicked[0], player_clicked[1], engine.board)
                        for i in range(len(valid_moves)):
                            if move == valid_moves[i]:
                                engine.make_move(valid_moves[i])
                                move_made = True
                                sq_selected = ()
                                player_clicked = []
                        if not move_made:
                            player_clicked = [sq_selected]
            elif e.type == p.KEYDOWN:
                if e.key == p.K_z:
                    engine.undo_move()
                    move_made = True
                    game_over = False
                if e.key == p.K_r:
                    engine = Engine.GameState()
                    valid_moves = engine.get_valid_moves()
                    move_made = False
                    sq_selected = ()
                    player_clicked = []
                    game_over = False

        # AI move engine
        if not game_over and not is_human:
            ai_move = BestMove.find_minimax_best_move(engine, valid_moves)
            if ai_move is None:
                print("this is random move")
                ai_move = BestMove.random_move(valid_moves)
            engine.make_move(ai_move)
            move_made = True

        if move_made:
            valid_moves = engine.get_valid_moves()
            move_made = False
        draw_game(screen, engine, valid_moves, sq_selected)

        if engine.check_mate:
            game_over = True
            if engine.player:
                draw_win_text(screen, c.BLACK_WIN)
            else:
                draw_win_text(screen, c.WHITE_WIN)
        elif engine.stale_mate:
            game_over = True
            draw_win_text(screen, c.STALE_MATE)

        clock.tick(c.MAX_FPS)
        p.display.flip()


def draw_win_text(screen, text):
    # ['arial', 'arialblack', 'bahnschrift', 'calibri', 'cambriacambriamath', 'cambria', 'candara', 'comicsansms', 'consolas', 'constantia']
    font = p.font.SysFont('consolas', 32, True, False)
    text_obj = font.render(text, 0, p.Color('red'))
    text_loc = p.Rect(0, 0, c.WIDTH, c.HEIGHT).move(c.WIDTH / 2 - text_obj.get_width() / 2,
                                                    c.HEIGHT / 2 - text_obj.get_height() / 2)
    screen.blit(text_obj, text_loc)


'''
Highlight the square that is possible move
'''


def highlight_sq(screen, engine, valid_moves, sq_selected):
    if sq_selected != ():
        opponent = ('b' if engine.player else 'w')
        row, col = sq_selected
        if engine.board[row][col][0] == ('w' if engine.player else 'b'):
            s = p.Surface((c.SQ_SIZE, c.SQ_SIZE))
            s.set_alpha(100)  # transparency
            s.fill(p.Color('blue'))
            screen.blit(s, (col * c.SQ_SIZE, row * c.SQ_SIZE))
            for move in valid_moves:
                if move.start_row == row and move.start_col == col:
                    if engine.board[move.end_row][move.end_col][0] == opponent:
                        s.fill(p.Color('red'))
                        screen.blit(s, (move.end_col * c.SQ_SIZE, move.end_row * c.SQ_SIZE))
                    else:
                        s.fill(p.Color('yellow'))
                        screen.blit(s, (move.end_col * c.SQ_SIZE, move.end_row * c.SQ_SIZE))


'''
Draw the game with current state
'''


def draw_game(screen, engine, valid_moves, sq_selected):
    draw_board(screen)
    highlight_sq(screen, engine, valid_moves, sq_selected)
    draw_piece(screen, engine.board)


'''
Draw the square board
'''


def draw_board(screen):
    """
    custom with own colors later on
    """
    colors = [p.Color("white"), p.Color("gray")]
    for row in range(c.DIMENSION):
        for column in range(c.DIMENSION):
            color = colors[((row + column) % 2)]
            # print(color)
            p.draw.rect(screen, color, p.Rect(column * c.SQ_SIZE, row * c.SQ_SIZE, c.SQ_SIZE, c.SQ_SIZE))


'''
Draw the the piece on the board
'''


def draw_piece(screen, board):
    for row in range(c.DIMENSION):
        for column in range(c.DIMENSION):
            piece = board[row][column]
            if piece != "--":
                screen.blit(c.IMAGES[piece], p.Rect(column * c.SQ_SIZE, row * c.SQ_SIZE, c.SQ_SIZE, c.SQ_SIZE))


if __name__ == '__main__':
    main()
